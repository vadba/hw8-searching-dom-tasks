// 1. Опишіть своїми словами що таке Document Object Model (DOM)

//      Обєктна модель документа являє собою структуроване подання документа та визначає те, як ця структура може бути доступна з програм, які можуть змінювати вміст, стиль та структуру документа. Подання DOM складається із структури елементів як об'єктів, які мають властивості та методи.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

//      innerText дозволяє ставити або отримувати текстовий вміст елемента та його нащадків. innerHTML - встановлює чи отримує HTML або XML розмітку дочірніх елементів.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

//      querySelector, querySelectorAll, getElementById, getElementsByName, getElementsByTagName, getElementsByClassName. querySelector, querySelectorAll - найбільш популярні та універсальні селектори.

const paragraf = document.querySelectorAll('p');

for (const iterator of paragraf) {
    iterator.style.backgroundColor = '#ff0000';
}

const list = document.querySelector('#optionsList');

console.log(list);

console.log(list.parentNode);

const child = list.childNodes;

console.log(child);

if (child) {
    for (const iterator of child) {
        console.log('Назва: ' + iterator.nodeName + ' -> ', 'Тип: ' + iterator.nodeType);
    }
}

const testParagraph = document.querySelector('#testParagraph');

testParagraph.textContent = 'This is a paragraph';

const mainHeader = document.querySelector('.main-header');

const mainHeaderChildren = mainHeader.children;

console.log(mainHeaderChildren);

if (mainHeaderChildren) {
    for (const iterator of mainHeaderChildren) {
        iterator.classList.add('nav-item');
    }
}

const sectionTitle = document.querySelectorAll('.section-title');

if (sectionTitle) {
    for (const iterator of sectionTitle) {
        iterator.classList.remove('section-title');
    }
}
